package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class Professor {

    private Long id;
    private String email;
    private String nome;
    private Double salario;
    private String telefone;
    private List<Seminario> seminario = new ArrayList<>();
    private Instituicao instituicao;

    public Professor(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public List<Seminario> getSeminarios() {
        return this.seminario;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return this.salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void adicionarSeminario(Seminario seminario) {
        this.seminario.add(seminario);
    }

    public boolean possuiSeminario(Seminario seminario) {
        return this.seminario.contains(seminario);
    }
}
